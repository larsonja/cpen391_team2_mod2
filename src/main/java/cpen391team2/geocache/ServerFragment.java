package cpen391team2.geocache;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.JsonReader;
import android.util.JsonToken;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ServerFragment extends Fragment implements AdapterView.OnItemClickListener,
  View.OnClickListener,
  WifiConnector.OwnerFragment {

  public interface ServerFragCommunicator {
    void requestWifiEnable();
    void downloadPackageFiles(String hostname, String packageName, List<String> filenames);
    void transferUserInfo();

    void showMapView(List<CachePackage> cachePackages);
  }

  private static final String PACKAGES_KEY = "packages";
  private static final String FILES_KEY = "files";
  private static final String SERVER_URL = "server_url";

  private ServerFragCommunicator comm;
  private TextView urlTextView;
  private ListView packagesFilesListView;
  private String currPackage = null;
  private List<String> files = null;
  private List<CachePackage> cachePackages = null;
  private PackageArrayAdapter adapter = null;

  private String currParams, currEmail;

  @Override
  public void wifiEnabledCallback() {
    downloadList(currParams);
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    CachePackage cp = (CachePackage) parent.getItemAtPosition(position);
    String packageName = cp.getName();
    ((MainActivity) getActivity()).setCachePackageName(cp.getName());
    ((MainActivity) getActivity()).setPackageProgress(cp.getProgress());

    currPackage = packageName;
    String params = MainActivity.DE2_RES_PATH + packageName;
    checkAndDownload(params);
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.server_frag_button_show_map:
        comm.showMapView(cachePackages);
        break;

      case R.id.server_frag_button_user_details_transfer:
        comm.transferUserInfo();
        break;

      default:
        // Nothing!
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // Assert that current logged in user email is not null
    String userEmail = ((MainActivity) getActivity()).getUserEmail();
    if (userEmail == null)
      getActivity().onBackPressed();
    else
      currEmail = userEmail;

    View v = inflater.inflate(R.layout.fragment_server, container, false);
    initialize(v);
    getActivity().setTitle(getResources().getString(R.string.app_name) + " - "
      + getResources().getString(R.string.server_frag_title));
    return v;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    if (savedInstanceState != null)
      urlTextView.setText(savedInstanceState.getString(SERVER_URL));
  }

  @Override
  public void onResume() {
    super.onResume();

    checkAndDownload(PACKAGES_KEY);
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(SERVER_URL, urlTextView.getText().toString());
  }

  private void initialize(View v) {
    WifiManager wifiManager = (WifiManager) getActivity().getSystemService(getActivity().WIFI_SERVICE);
    if (wifiManager == null) {
      Toast toast = Toast.makeText(getActivity(), "No Wifi Service found on Device!", Toast.LENGTH_LONG);
      toast.show();
      getActivity().onBackPressed();
      return;
    }

    urlTextView = (TextView) v.findViewById(R.id.server_frag_textview_url);
    packagesFilesListView = (ListView) v.findViewById(R.id.server_frag_listview_packages_files_list);

    if (files == null)
      files = new ArrayList<>();
    if (cachePackages == null)
      cachePackages = new ArrayList<>();

    v.findViewById(R.id.server_frag_button_show_map).setOnClickListener(this);
    v.findViewById(R.id.server_frag_button_user_details_transfer).setOnClickListener(this);
  }

  public void enableWifi(boolean success) {
    if (success) {
      WifiConnector wifiConnector = new WifiConnector(getActivity(), this);
      wifiConnector.enableWifi();
    } else {
      Toast toast = Toast.makeText(getActivity(), "Cannot Connect to Server without Wifi!", Toast.LENGTH_LONG);
      toast.show();
      getActivity().onBackPressed();
    }
  }

  private void populatePackagesListView() {
    if (adapter == null) {
      adapter = new PackageArrayAdapter(getActivity(),
        android.R.layout.simple_expandable_list_item_1,
        cachePackages);
    }

    packagesFilesListView.setAdapter(adapter);
    packagesFilesListView.setOnItemClickListener(this);
    adapter.notifyDataSetChanged();
  }

  private void checkAndDownload(String params) {
    WifiManager wifiManager = (WifiManager) getActivity().getSystemService(getActivity().WIFI_SERVICE);
    if (wifiManager.isWifiEnabled())
      downloadList(params);
    else {
      currParams = params;
      comm.requestWifiEnable();
    }
  }

  private void downloadList(String params) {
    String urlPrefix = urlTextView.getText().toString();
    if (!urlPrefix.endsWith("/"))
      urlPrefix += "/";

    ProgressDialog progDialog = initProgressDialog();

    final JsonTask jsonTask = new JsonTask(progDialog);
    jsonTask.execute(urlPrefix + params + "?user=" + currEmail);

    progDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        jsonTask.cancel(true);
      }
    });
  }

  private ProgressDialog initProgressDialog() {
    ProgressDialog progDialog = new ProgressDialog(getActivity());
    progDialog.setMessage("Retrieving Package Information from Server...");
    progDialog.setIndeterminate(true);
    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    progDialog.setCancelable(true);
    progDialog.setCanceledOnTouchOutside(false);
    return progDialog;
  }

  @SuppressWarnings("deprecation")
  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    try {
      comm = (ServerFragCommunicator) activity;
    } catch (ClassCastException e) {
      throw new ClassCastException(comm.toString() + " must implement ServerFragCommunicator");
    }
  }

  private class JsonTask extends AsyncTask<String, Integer, String> {
    private ProgressDialog progDialog;
    private PowerManager.WakeLock mWakeLock;

    private JsonTask(ProgressDialog progDialog) {
      this.progDialog = progDialog;
    }

    @Override
    protected String doInBackground(String... sUrls) {
      InputStream input = null;
      JsonReader jsonReader = null;
      HttpURLConnection connection = null;
      String type;

      try {
        URL url = new URL(sUrls[0]); //TODO will only be one, but make sure to refactor later
        connection = (HttpURLConnection) url.openConnection();
        connection.connect();

        // expect HTTP 200 OK, so we don't mistakenly save error report
        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
          return null;
        }

        input = new BufferedInputStream(connection.getInputStream());
        jsonReader = new JsonReader(new InputStreamReader(input));
        jsonReader.beginObject();

        type = jsonReader.nextName();
        switch (type) {
          case PACKAGES_KEY:
            readPackagesJson(jsonReader);
            break;
          case FILES_KEY:
            readFilesJson(jsonReader);
            break;
          default:
            return null;
        }

      } catch (Exception e) {
        return null;

      } finally {
        try {
          if (input != null)
            input.close();
          if (jsonReader != null)
            jsonReader.close();
        } catch (IOException ignored) {
        }

        if (connection != null)
          connection.disconnect();
      }
      return type;
    }

    private void readPackagesJson(JsonReader jsonReader) throws IOException {
      cachePackages.clear();
      jsonReader.beginArray();
      while (jsonReader.hasNext()) {
        jsonReader.beginObject();

        String packageName = null, description = null;
        int progress = 0, numCaches = 0;
        double lat = 0.0, lon = 0.0;
        while (!jsonReader.peek().equals(JsonToken.END_OBJECT)) {
          String prop = jsonReader.nextName();
          switch (prop) {
            case "name":
              packageName = jsonReader.nextString();
              break;

            case "description":
              description = jsonReader.nextString();
              break;

            case "progress":
              progress = jsonReader.nextInt();
              break;

            case "numCaches":
              numCaches = jsonReader.nextInt();
              break;

            case "lat":
              lat = jsonReader.nextDouble();
              break;

            case "lon":
              lon = jsonReader.nextDouble();
              break;

            default:
              // Nothing! Unhandled Property found in Result Json!
          }
        }

        jsonReader.endObject();
        cachePackages.add(new CachePackage(packageName, description, progress, numCaches, lat, lon));
      }
      jsonReader.endArray();
    }

    private void readFilesJson(JsonReader jsonReader) throws IOException {
      files.clear();
      jsonReader.beginArray();
      while (jsonReader.hasNext()) {
        String next = jsonReader.nextString();
        files.add(next);
      }
    }

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      PowerManager pm = (PowerManager) getActivity().getSystemService(getActivity().POWER_SERVICE);
      mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
        getClass().getName());
      mWakeLock.acquire();
      progDialog.show();
    }

    @Override
    protected void onPostExecute(String result) {
      mWakeLock.release();
      progDialog.dismiss();
      if (result == null)
        Toast.makeText(getActivity(), "Error Connecting to Server!", Toast.LENGTH_LONG).show();
      else {
        switch (result) {
          case PACKAGES_KEY:
            populatePackagesListView();
            break;
          case FILES_KEY:
            if (currPackage != null)
              comm.downloadPackageFiles(urlTextView.getText().toString(), currPackage, files);
            break;
          default:
            // Do Nothing
        }
      }
    }
  }
}
