package cpen391team2.geocache;

/**
 * Created by adam on 05/04/16.
 */
public class CachePackage {
  private String name, description;
  private int progress, total;
  private double lat, lon;

  public CachePackage(String name, String description, int progress, int total, double lat, double lon) {
    this.name = name;
    this.description = description;
    this.progress = progress;
    this.total = total;
    this.lat = lat;
    this.lon = lon;
  }

  public String getName() { return name; }

  public String getDescription() { return description; }

  public int getProgress() { return progress; }

  public int getNumCaches() { return total; }

  public double getLat() { return lat; }

  public double getLon() { return lon; }

  @Override
  public String toString() {
    // Does not stringify the lat/lon decimal degrees coordinates!
    return this.name + " " + this.progress + "/" + this.total;
  }
}
