package cpen391team2.geocache;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

public class BluetoothFragment extends Fragment implements AdapterView.OnItemClickListener {

  public interface BluetoothFragCommunicator {
    void onBluetoothConnected();

    void onFileTransferComplete(String err);
    void onReceiveUserInfoComplete(String err);
  }

  private final static byte[] READY_BYTE_ARRAY = {'U', 'P', 'D', '\0'};
  private final static byte[] PRO_BYTE_ARRAY = {'P', 'R', 'O', '\0'};
  private final static byte[] DONE_BYTE_ARRAY = {'E', 'O', 'U', '\0'};
  private final static byte[] USER_BYTE_ARRAY = {'U', '\0'};
  private final static byte[] PROGRESS_BYTE_ARRAY = {'P', '\0'};
  private final static byte[] PACKAGE_BYTE_ARRAY = {'K', '\0'};
  private final static String RDY_STRING = "RDY\0";
  private final static String ACK_STRING = "ACK\0";
  private final static String EOU_STRING = "EOU\0";
//  private final static int MAX_SEND_SIZE = 16;
  private final static int MAX_READ_TIMEOUT = 200;
  private final static int BYTE_BUF_SIZE = 4096;
  private final static int REQUEST_ENABLE_BT = 1;
  private boolean connected = false;
  private Context context;

  private BluetoothAdapter mBluetoothAdapter;
  private BluetoothSocket mmSocket = null;
  private BroadcastReceiver mReceiver = null;
  private BluetoothFragCommunicator comm;
  private MyCustomArrayAdapter myDiscoveredArrayAdapter;

  private ArrayList<BluetoothDevice> discoveredDevices = new ArrayList<>();
  private ArrayList<String> myDiscoveredDevicesStringArray = new ArrayList<>();

  private IntentFilter filterFound = null;
  private IntentFilter filterStart = null;
  private IntentFilter filterStop = null;

  // Input/Output streams to for writing to the local storage
  public static InputStream mmInStream = null;
  public static OutputStream mmOutStream = null;


  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_bluetooth, container, false);
    context = getActivity();
    getActivity().setTitle(getResources().getString(R.string.app_name) + " - "
      + getResources().getString(R.string.bluetooth_frag_title));
    initialize(v);
    return v;
  }

  @Override
  public void onResume() {
    super.onResume();

    if (mBluetoothAdapter != null)
      mBluetoothAdapter.startDiscovery();

    if (mReceiver != null) {
      if (filterFound != null)
        context.registerReceiver(mReceiver, filterFound);
      if (filterStart != null)
        context.registerReceiver(mReceiver, filterStart);
      if (filterStop != null)
        context.registerReceiver(mReceiver, filterStop);
    }

    // Clear the list of discovered devices when fragment is suspended and recreated
    if (discoveredDevices.size() > 0) {
      discoveredDevices.clear();
      myDiscoveredDevicesStringArray.clear();
    }
    Log.i("bttag", "instream: " + (mmInStream != null));
    Log.i("bttag", "outstream: " + (mmOutStream != null));
  }

  @Override
  public void onPause() {
    super.onPause();
    if (mReceiver != null)
      context.unregisterReceiver(mReceiver);

    if (mBluetoothAdapter != null)
      mBluetoothAdapter.cancelDiscovery();

    if (connected)
      closeConnection();
  }

  private void initialize(View v) {
    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    if (mBluetoothAdapter == null) {
      Toast toast = Toast.makeText(context, "No Bluetooth", Toast.LENGTH_LONG);
      toast.show();
      getActivity().onBackPressed();
      return;
    }

    if (mReceiver == null) {
      mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
          String action = intent.getAction();

          switch (action){
            case BluetoothDevice.ACTION_FOUND :
              BluetoothDevice newDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
              String newDeviceString = newDevice.getName()
                + "\nMAC Address = " + newDevice.getAddress();
              Log.i("bttag", newDeviceString);

              discoveredDevices.add(newDevice);
              myDiscoveredDevicesStringArray.add(newDeviceString);
              myDiscoveredArrayAdapter.notifyDataSetChanged();
              break;

            case BluetoothAdapter.ACTION_DISCOVERY_STARTED :
              Log.i("bttag", "Discovery Started");
              break;

            case BluetoothAdapter.ACTION_DISCOVERY_FINISHED :
              Log.i("bttag", "Discovery Finished");
              break;
          }
        }
      };
    }

    createFiltersIfNull();
    createArrayAdaptersIfNull();
    initGUIDatabases(v);

    ensureBTAdapterEnabled();
    logDevices();
  }

  private void createFiltersIfNull() {
    if (filterFound == null)
      filterFound = new IntentFilter(BluetoothDevice.ACTION_FOUND);
    if (filterStart == null)
      filterStart = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
    if (filterStop == null)
      filterStop = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
  }

  private void createArrayAdaptersIfNull() {
    if (myDiscoveredArrayAdapter == null)
      myDiscoveredArrayAdapter = new MyCustomArrayAdapter(context,
        android.R.layout.simple_list_item_1, myDiscoveredDevicesStringArray);
  }

  private void initGUIDatabases(View v) {
    ListView discoveredListView = (ListView) v.findViewById(R.id.bluetooth_frag_listview_discovered_devices);

    discoveredListView.setOnItemClickListener(this);
    discoveredListView.setAdapter(myDiscoveredArrayAdapter);
  }

  private void ensureBTAdapterEnabled() {
    if (!mBluetoothAdapter.isEnabled()) {
      Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
      startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }
  }

  private void logDevices() {
    Log.i("mytag", "discoverdDevices size: " + discoveredDevices.size());
    Log.i("mytag", "myDiscoveredDevicesStringArray size: " + myDiscoveredDevicesStringArray.size());
  }

  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == REQUEST_ENABLE_BT) {
      if (resultCode != Activity.RESULT_OK) {
        Toast.makeText(context, "BlueTooth Failed to Start!", Toast.LENGTH_LONG).show();
        getActivity().onBackPressed();
      }
    }
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    switch (parent.getId()) {
      case R.id.bluetooth_frag_listview_discovered_devices:
        String discoveredDeviceName = "Connecting to Device: " +
          myDiscoveredDevicesStringArray.get(position);
        Log.i("bttag", discoveredDeviceName);

        if (connected)
          closeConnection();

        createSerialBluetoothDeviceSocket(discoveredDevices.get(position));
        connectToSerialBlueToothDevice();
        myDiscoveredArrayAdapter.notifyDataSetChanged();
        break;

      default:
    }
  }

  private void closeConnection() {
    try {
      mmInStream.close();
      mmInStream = null;
    } catch (IOException e) {
      Log.i("cCon", e.toString());
    }
    try {
      mmOutStream.close();
      mmOutStream = null;
    } catch (IOException e) {
      Log.i("cCon", e.toString());
    }
    try {
      mmSocket.close();
      mmSocket = null;
    } catch (IOException e) {
      Log.i("cCon", e.toString());
    }
    connected = false;
  }

  private void createSerialBluetoothDeviceSocket(BluetoothDevice device) {
    mmSocket = null;

    // universal UUID for a serial profile RFCOMM blue tooth device
    // this is just one of those “things” that you have to do and just works
    UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    try {
      mmSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
    } catch (IOException e) {
      Log.i("bttag", "Socket Creation Failed");
    }
  }

  private void connectToSerialBlueToothDevice() {
    mBluetoothAdapter.cancelDiscovery();
    try {
      mmSocket.connect();
      Log.i("bttag", "Connection Made");
    } catch (IOException connectException) {
      Log.i("bttag", "Connection Failed");
      return;
    }

    getInputOutputStreamsForSocket();
    connected = true;
    comm.onBluetoothConnected();
  }

  private void getInputOutputStreamsForSocket() {
    try {
      mmInStream = mmSocket.getInputStream();
      mmOutStream = mmSocket.getOutputStream();
    } catch (IOException e) {
      Log.i("inStream", e.toString());
    }
  }

//------------------------------------------------------------------------------------------------//

  public void sendFileToBTDevice(Map<String, Long> fileSizes, String... filesnames) {
    ProgressDialog progDialog = new ProgressDialog(getActivity());
    progDialog.setMessage("Sending files to DE2 through Bluetooth...");
    progDialog.setIndeterminate(true);
    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    progDialog.setCancelable(true);
    progDialog.setCanceledOnTouchOutside(false);

    final BTTask task = new BTTask(progDialog, fileSizes);
    task.execute(filesnames);
  }

  public String readFromBTDevice() {
    byte c;
    String s = "";
    int time = 0;

    try { // Read from the InputStream using polling and timeout
      while (true) {
        if (time > MAX_READ_TIMEOUT)
          break;

        if (mmInStream.available() > 0) {
          time = 0;
          if ((c = (byte) mmInStream.read()) != '\0') // '\0' terminator
            s += (char) c; // build up string 1 byte by byte
          else {
            return s + '\0';
          }
        } else {
          SystemClock.sleep(10);
          ++time;
        }
      }
    } catch (IOException e) {
      return "-- No Response --";
    }
    return s;
  }

  @SuppressWarnings("deprecation")
  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    try {
      comm = (BluetoothFragCommunicator) activity;
    } catch (ClassCastException e) {
      throw new ClassCastException(comm.toString() + " must implement BluetoothFragCommunicator");
    }
  }

  protected void receiveProgress() {
    ProgressDialog progDialog = new ProgressDialog(getActivity());
    progDialog.setMessage("Receiving files from DE2 through Bluetooth...");
    progDialog.setIndeterminate(true);
    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    progDialog.setCancelable(true);
    progDialog.setCanceledOnTouchOutside(false);

    final BTTask task = new BTTask(progDialog, null);
    task.execute();
  }

  private String receiveProgressAsync() {
    String logTagHandshake = "userProgress";
    Log.i(logTagHandshake, "startP");
    try {
      mmOutStream.write(PRO_BYTE_ARRAY);
      waitOnResponse(RDY_STRING, logTagHandshake, "In UPD loop");

      // Slice of the last character of received Strings due to trailing null character
      writeAndLog(USER_BYTE_ARRAY, logTagHandshake, "Starting Email");
      String usersEmail = waitOnResponseVerify(USER_BYTE_ARRAY, logTagHandshake, "Email loop");
      if (usersEmail != null)
        ((MainActivity) getActivity()).setUserEmail(usersEmail.substring(0, usersEmail.length() - 1));
      else
        Log.i(logTagHandshake, "Error Receiving User Email!");

      writeAndLog(PROGRESS_BYTE_ARRAY, logTagHandshake, "Starting progress");
      String usersProgress = waitOnResponseVerify(PROGRESS_BYTE_ARRAY, logTagHandshake, "Progress loop");
      if (usersProgress != null)
        ((MainActivity) getActivity()).setPackageProgress(Integer.parseInt(usersProgress.substring(0, usersProgress.length() - 1)));
      else
        Log.i(logTagHandshake, "Error Receiving Progress Count!");

      writeAndLog(PACKAGE_BYTE_ARRAY, logTagHandshake, "Starting package");
      String usersPackage = waitOnResponseVerify(PACKAGE_BYTE_ARRAY, logTagHandshake, "Package loop");
      if (usersPackage != null)
        ((MainActivity) getActivity()).setCachePackageName(usersPackage.substring(0, usersPackage.length() - 1));
      else
        Log.i(logTagHandshake, "Error Receiving Package Name!");

      writeAndLog(DONE_BYTE_ARRAY, logTagHandshake, "Done byte array");
      waitOnResponse(EOU_STRING, logTagHandshake, "In last Loop");

    } catch (IOException e) {
      return e.getMessage();
    }

    // Receive Progress Success!
    Log.i("bbb", "Received user email: " + ((MainActivity) getActivity()).getUserEmail());
    Log.i("bbb", "Received user package: " + ((MainActivity) getActivity()).getCachePackageName());
    Log.i("bbb", "Received user progress: " + ((MainActivity) getActivity()).getProgress());
    return null;
  }

  private String waitOnResponseVerify(byte[] code, String logTag, String logMessage) {

    if(Arrays.equals(code, USER_BYTE_ARRAY)){
      Log.i(logTag, "In User case " + logMessage);
      return parseUserData("@", ".com");
    } else if(Arrays.equals(code, PROGRESS_BYTE_ARRAY)) {
      Log.i(logTag, "In Progress case " + logMessage);
      return parseUserData("", "");
    } else if(Arrays.equals(code, PACKAGE_BYTE_ARRAY)) {
      Log.i(logTag, "In Package case " + logMessage);
      return parseUserData("package", "");
    }
    return null;
  }

  private String parseUserData(String arg1, String arg2) {
    String response;
    Boolean notFinished = true;

    response = readFromBTDevice();
    return response;
  }

  private String writeAndLog(byte[] writeStr, String logTag, String logMsg){
    try {
      mmOutStream.write(writeStr);
      Log.i(logTag, Arrays.toString(writeStr) + " " + logMsg);
    } catch (IOException e){
      return e.getMessage();
    }
    return null;
  }

  private void waitOnResponse(String expectedResponse, String logTag, String logLoop) {
    String response;
    do {
      response = readFromBTDevice();
      Log.i(logTag, response + " " + logLoop);
    } while (!response.equals(expectedResponse));
  }

  private class BTTask extends AsyncTask<String, Integer, String> {
    private ProgressDialog progDialog;
    private PowerManager.WakeLock mWakeLock;
    private Map<String, Long> fileSizes;

    private BTTask(ProgressDialog progDialog, Map<String, Long> fileSizes) {
      this.progDialog = progDialog;
      this.fileSizes = fileSizes;
    }

    @Override
    protected String doInBackground(String... filesnames) {
      if (fileSizes == null) {
        // fileSizes is null meaning this async task needs to receive user progress
        return receiveProgressAsync();
      }

      String logTagHandshake = "asd";
      Log.i(logTagHandshake, "startF");
      try {
        mmOutStream.write(READY_BYTE_ARRAY);
        waitOnResponse(RDY_STRING, logTagHandshake, "In UPD loop");

        for (String file : filesnames) {
          String stringMsg = file.charAt(0) + "" + fileSizes.get(file) + '\0';
          writeAndLog(stringMsg.getBytes(), logTagHandshake, "String Message");
          waitOnResponse(stringMsg, logTagHandshake, "In long thing string");

          InputStream in = context.openFileInput(file);
          byte data[] = new byte[BYTE_BUF_SIZE];

          int len = in.read(data);
          while (len == BYTE_BUF_SIZE) {
            Log.i(logTagHandshake, "len: " + len);
            mmOutStream.write(data);
            len = in.read(data);
          }
          if (len > 0) {
            mmOutStream.write(Arrays.copyOfRange(data, 0, len));
          }
          waitOnResponse(ACK_STRING, logTagHandshake, "In ACK loop");
        }

        String usersEmail = ((MainActivity) getActivity()).getUserEmail();
        int emailLength = usersEmail.length();
        String emailStr = ("U" + emailLength + '\0');

        writeAndLog(emailStr.getBytes(), logTagHandshake, "Email Length String");
        waitOnResponse(emailStr, logTagHandshake, "In U## loop");

        writeAndLog(usersEmail.getBytes(), logTagHandshake, "Email String");
        waitOnResponse(ACK_STRING, logTagHandshake, "In email name");

        int userProgress = ((MainActivity) getActivity()).getProgress();
        String progressStr = ("P" + userProgress + '\0');

        writeAndLog(progressStr.getBytes(), logTagHandshake, "Progress String");
        waitOnResponse(progressStr, logTagHandshake, "In progress string");

        String userPackage = ((MainActivity) getActivity()).getCachePackageName();
        int packageLen = userPackage.length();
        String userPkg = ("K" + packageLen + '\0');

        writeAndLog(userPkg.getBytes(), logTagHandshake, "Package Length");
        waitOnResponse(userPkg, logTagHandshake, "In package length");

        writeAndLog(userPackage.getBytes(), logTagHandshake, "Package String");
        waitOnResponse(ACK_STRING, logTagHandshake, "In package string");

        writeAndLog(DONE_BYTE_ARRAY, logTagHandshake, "Done byte array");
        waitOnResponse(EOU_STRING, logTagHandshake, "In last Loop");

      } catch (IOException e) {
        return e.getMessage();
      }
      return null;
    }

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      PowerManager pm = (PowerManager) getActivity().getSystemService(getActivity().POWER_SERVICE);
      mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
        getClass().getName());
      mWakeLock.acquire();
      progDialog.show();
    }

    @Override
    protected void onPostExecute(String result) {
      mWakeLock.release();
      progDialog.dismiss();
      if (result != null)
        Toast.makeText(getActivity(), "Error Transferring Files! Error: " + result, Toast.LENGTH_LONG).show();
      else {
        Toast.makeText(getActivity(), "BlueTooth Transfer Success!", Toast.LENGTH_LONG).show();

        if (fileSizes == null) {
          // This AsyncTask was responsible for receiving user progress from DE2
          comm.onReceiveUserInfoComplete(null);
        } else {
          // This AsyncTask was responsible for sending static package files to DE2
          comm.onFileTransferComplete(null);
        }
      }
    }

  }
}


