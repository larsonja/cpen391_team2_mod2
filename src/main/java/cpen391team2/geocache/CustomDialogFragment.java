package cpen391team2.geocache;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Map;

public class CustomDialogFragment extends DialogFragment {

  public enum DialogKeys {
    TITLE,
    MESSAGE,
    NEG_BUTTON,
    NEU_BUTTON,
    NEU_BUTTON_CALLBACK,
    POS_BUTTON,
    POS_BUTTON_CALLBACK,
    CUSTOM_LAYOUT_RES
  }

  private CustomDialogListener dialogListener;

  interface CustomDialogListener {
    void doButtonClick(String buttonText);
  }

  public static CustomDialogFragment newInstance(Map<DialogKeys, String> mapBundle) {
    CustomDialogFragment f = new CustomDialogFragment();
    Bundle b = new Bundle();

    for (DialogKeys key : mapBundle.keySet())
      b.putCharSequence(key.toString(), mapBundle.get(key));

    f.setArguments(b);
    return f;
  }

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

    for (DialogKeys key : DialogKeys.values()) {
      final String keyString = key.toString();
      if (getArguments().containsKey(keyString)) switch (key) {
        case TITLE:
          builder.setTitle(getArguments().getString(keyString));
          break;

        case MESSAGE:
          builder.setMessage(getArguments().getString(keyString));
          break;

        case NEG_BUTTON:
          initNegButton(builder, keyString);
          break;

        case POS_BUTTON:
          initPosButton(builder, keyString);
          break;

        case NEU_BUTTON:
          initNeuButton(builder, keyString);
          break;

        default:
      }
    }

    if (getArguments().containsKey(DialogKeys.CUSTOM_LAYOUT_RES.toString())) {
      setUpCustomLayout(builder);
    }
    return builder.create();
  }

  private void initNegButton(AlertDialog.Builder builder, final String keyString) {
    builder.setNegativeButton(getArguments().getString(keyString), new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        dialogListener.doButtonClick(getArguments().getString(keyString));
      }
    });
  }

  private void initPosButton(AlertDialog.Builder builder, final String keyString) {
    builder.setPositiveButton(getArguments().getString(keyString), new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        if (!getArguments().containsKey(DialogKeys.POS_BUTTON_CALLBACK.toString()))
          dismiss();
        else
          dialogListener.doButtonClick(getArguments().getString(keyString));
      }
    });
  }

  private void initNeuButton(AlertDialog.Builder builder, final String keyString) {
    builder.setNeutralButton(getArguments().getString(keyString), new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        if (!getArguments().containsKey(DialogKeys.NEU_BUTTON_CALLBACK.toString()))
          dismiss();
        else {
          ((MainActivity) getActivity()).setFileSelectedId(((RadioGroup) getDialog().findViewById(R.id.radio_group_files)).getCheckedRadioButtonId());
          dialogListener.doButtonClick(getArguments().getString(keyString));
        }
      }
    });
  }

  private void setUpCustomLayout(AlertDialog.Builder builder) {
    LayoutInflater i = getActivity().getLayoutInflater();
    View dialogView = i.inflate(((MainActivity) getActivity()).getLayoutRes(), null);
    TextView packageTextView = (TextView) dialogView.findViewById(R.id.radio_group_files_package);
    String toSetText = "Current Package: " + ((MainActivity) getActivity()).getCurrPackage();
    packageTextView.setText(toSetText);
    builder.setView(dialogView);
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    try {
      dialogListener = (CustomDialogListener) activity;
    } catch (ClassCastException e) {
      throw new ClassCastException(dialogListener.toString() + " must implement CustomDialogListener");
    }
  }
}
