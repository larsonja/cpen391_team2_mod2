package cpen391team2.geocache;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

/**
 * Created by adam on 05/04/16.
 */
public class PackageArrayAdapter extends ArrayAdapter<CachePackage> {

  private Context context;
  private List<CachePackage> cachePackages;

  public PackageArrayAdapter(Context context, int resource, List<CachePackage> cachePackages) {
    super(context, resource, cachePackages);
    this.context = context;
    this.cachePackages = cachePackages;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(
      Context.LAYOUT_INFLATER_SERVICE);
    if (convertView == null)
      convertView = inflater.inflate(R.layout.package_row, parent, false);

    TextView nameTextView = (TextView) convertView.findViewById(R.id.package_row_textview_package_name);
    TextView descriptionTextView = (TextView) convertView.findViewById(R.id.package_row_textview_package_description);
    TextView progressTextView = (TextView) convertView.findViewById(R.id.package_row_textview_user_progress);
    TextView numCachesTextView = (TextView) convertView.findViewById(R.id.package_row_textview_num_caches);

    CachePackage pack = cachePackages.get(position);
    nameTextView.setText(pack.getName());
    descriptionTextView.setText(pack.getDescription());
    progressTextView.setText("" + pack.getProgress());
    numCachesTextView.setText("" + pack.getNumCaches());

    return convertView;
  }
}
