package cpen391team2.geocache;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Nick on 4/5/2016.
 */
public class MyMapFragment extends Fragment implements OnMapReadyCallback {

  private static String COORD_PREFIX = "coord_";
  private static String DESC_PREFIX = "desc_";
  private static String PACK_LIST = "packages_list";
  private MapView mMapView;

  public static MyMapFragment newInstance(List<CachePackage> cachePackages) {
    MyMapFragment myMapFragment = new MyMapFragment();
    Bundle b = new Bundle();

    List<String> packageNames = new ArrayList<>();
    for (CachePackage cp: cachePackages) {
      double[] coords ={
        cp.getLat(),
        cp.getLon()
      };
      b.putDoubleArray(COORD_PREFIX + cp.getName(), coords);
      b.putString(DESC_PREFIX + cp.getName(), cp.getDescription());
      packageNames.add(cp.getName());
    }
    b.putStringArrayList(PACK_LIST, (ArrayList<String>) packageNames);

    myMapFragment.setArguments(b);
    return myMapFragment;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_map, container,
      false);
    getActivity().setTitle(getResources().getString(R.string.app_name) + " - "
      + getResources().getString(R.string.map_frag_title));
    mMapView = (MapView) v.findViewById(R.id.mapView);
    mMapView.onCreate(savedInstanceState);

    mMapView.onResume();// needed to get the map to display immediately

    try {
      MapsInitializer.initialize(getActivity().getApplicationContext());
    } catch (Exception e) {
      e.printStackTrace();
    }

    mMapView.getMapAsync(this);

    // Perform any camera updates here
    return v;
  }

  @Override
  public void onResume() {
    super.onResume();
    mMapView.onResume();
  }

  @Override
  public void onPause() {
    super.onPause();
    mMapView.onPause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    mMapView.onDestroy();
  }

  @Override
  public void onLowMemory() {
    super.onLowMemory();
    mMapView.onLowMemory();
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    Bundle args = getArguments();
    List<String> cachePackageNames = args.getStringArrayList(PACK_LIST);
    for (String packageName: cachePackageNames) {
      double[] coords = args.getDoubleArray(COORD_PREFIX + packageName);
      String description = args.getString(DESC_PREFIX + packageName);

      // create marker
      MarkerOptions marker = new MarkerOptions()
                                 .position(new LatLng(coords[0], coords[1]))
                                 .title(packageName)
                                 .snippet(description);

      // Changing marker icon
      marker.icon(BitmapDescriptorFactory
        .defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

      // adding marker
      googleMap.addMarker(marker);
    }

    // Our latitude and longitude at UBC
    // Set map initial position to be centered around our coordinates at UBC
    double latitude = 49.264675;
    double longitude = -123.246018;

    CameraPosition cameraPosition = new CameraPosition.Builder()
      .target(new LatLng(latitude, longitude)).zoom(12).build();
    googleMap.animateCamera(CameraUpdateFactory
      .newCameraPosition(cameraPosition));
  }
}


