package cpen391team2.geocache;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * Created by adam on 05/04/16.
 */
public class WifiConnector {

  public interface OwnerFragment {
    void wifiEnabledCallback();
  }

  private Activity activity;
  private OwnerFragment ownerFragment;

  private IntentFilter wifiFilter = null;
  private BroadcastReceiver wifiReceiver = null;
  private ProgressDialog wifiDialog = null;

  public WifiConnector(Activity activity, Fragment fragment) {
    try {
      ownerFragment = (OwnerFragment) fragment;
    } catch (ClassCastException e) {
      throw new ClassCastException(ownerFragment.toString() + " must implement OwnerFragment");
    }

    this.activity = activity;
  }

  public void enableWifi() {
    if (wifiDialog == null) {
      initWifiDialog();
    }

    if (wifiFilter == null) {
      initWifiFilter();
    }

    if (wifiReceiver == null) {
      initWifiReceiver();
    }
    activity.registerReceiver(wifiReceiver, wifiFilter);
    wifiDialog.show();
    WifiManager wifiManager = (WifiManager) activity.getSystemService(activity.WIFI_SERVICE);
    wifiManager.setWifiEnabled(true);
  }

  private void initWifiDialog() {
    wifiDialog = new ProgressDialog(activity);
    wifiDialog.setMessage("Attempting to Connect to Wifi...");
    wifiDialog.setIndeterminate(true);
    wifiDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    wifiDialog.setCancelable(false);
  }

  private void initWifiFilter() {
    wifiFilter = new IntentFilter();
    wifiFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
  }

  private void initWifiReceiver() {
    wifiReceiver = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
          NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);

          if (info != null && info.isConnectedOrConnecting()) {
            if (info.isConnected()) {
              wifiDialog.dismiss();
              activity.unregisterReceiver(wifiReceiver);
              Log.i("wifitag", "connected to network");
              ownerFragment.wifiEnabledCallback();

            } else {
              Log.i("wifitag", "in the middle of connecting");
            }
          }
        }
      }
    };
  }
}
