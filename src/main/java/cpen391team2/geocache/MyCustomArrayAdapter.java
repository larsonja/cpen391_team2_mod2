package cpen391team2.geocache;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MyCustomArrayAdapter extends ArrayAdapter<String> {

  public final int numRows = 500;
  private boolean[] rowValidity = new boolean[numRows];


  private Context context;
  private ArrayList<String> stringArray;

  public MyCustomArrayAdapter(Context context, int textViewResourceId, ArrayList<String> stringArray) {
    // call base class constructor
    super(context, textViewResourceId, stringArray);
    this.context = context;
    this.stringArray = stringArray;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(
      Context.LAYOUT_INFLATER_SERVICE);
    View row = inflater.inflate(R.layout.row, parent, false);

    ImageView icon = (ImageView) row.findViewById(R.id.BTicon);
    icon.setImageResource(R.drawable.bluetooth);
    icon.setVisibility(View.VISIBLE);

    TextView label = (TextView) row.findViewById(R.id.BTdeviceText);
    label.setText(stringArray.get(position));

    icon = (ImageView) row.findViewById(R.id.Selected);
    icon.setImageResource(R.drawable.redcross);
    icon.setVisibility(View.VISIBLE);

    if (!rowValidity[position])
      icon.setImageResource(R.drawable.redcross);
    else
      icon.setImageResource(R.drawable.greentick);
    return row;
  }

//  public void setValid(int position) {
//    rowValidity[position] = true;
//  }

//  public void setInValid(int position) {
//    rowValidity[position] = false;
//  }
}
