package cpen391team2.geocache;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.JsonReader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adam on 01/04/16.
 */
public class LoginFragment extends Fragment implements View.OnClickListener,
                                                       AdapterView.OnItemSelectedListener,
                                                       WifiConnector.OwnerFragment {

  private static final int METHOD_GET_USERS = 1;
  private static final int METHOD_CREATE_USER = 2;
  private static final int METHOD_DELETE_USER = 3;

  private Spinner accListSpinner;
  private List<String> accListSpinnerArray;
  private ArrayAdapter<String> accListSpinnerArrayAdapter;

  private EditText newUserEmailEditText;

  LoginFragCommunicator comm;

  @Override
  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    ((MainActivity) getActivity()).setUserEmail(parent.getItemAtPosition(position).toString());
  }

  @Override
  public void onNothingSelected(AdapterView<?> parent) {
    // Nothing special needs to be done here!
  }

  public interface LoginFragCommunicator {
    void signinButtonClicked();
    void requestWifiEnable();
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    try {
      comm = (LoginFragCommunicator) activity;
    } catch (ClassCastException e) {
      throw new ClassCastException(comm.toString() + " must implement MainCommunicator");
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_login, container, false);
    initialize(v);
    getActivity().setTitle(getResources().getString(R.string.app_name) + " - "
      + getResources().getString(R.string.login_frag_title));

    return v;
  }

  private void initialize(View v) {
    if (accListSpinnerArray == null)
      accListSpinnerArray = new ArrayList<>();
    else
      accListSpinnerArray.clear();

    accListSpinner = (Spinner) v.findViewById(R.id.login_frag_acc_list_spinner);

    if (accListSpinnerArrayAdapter == null) {
      accListSpinnerArrayAdapter =
        new ArrayAdapter<>(getActivity(),
          android.R.layout.simple_spinner_dropdown_item,
          accListSpinnerArray);
    }

    accListSpinner.setAdapter(accListSpinnerArrayAdapter);
    accListSpinner.setOnItemSelectedListener(this);

    if (newUserEmailEditText == null)
      newUserEmailEditText = (EditText) v.findViewById(R.id.login_frag_create_edit_text);

    v.findViewById(R.id.login_frag_button_create).setOnClickListener(this);
    v.findViewById(R.id.login_frag_button_signin).setOnClickListener(this);
    v.findViewById(R.id.login_frag_button_delete).setOnClickListener(this);

    checkAndDownload();
  }

  private void checkAndDownload() {
    WifiManager wifiManager = (WifiManager) getActivity().getSystemService(getActivity().WIFI_SERVICE);
    if (wifiManager.isWifiEnabled())
      getUsersFromServer();
    else
      comm.requestWifiEnable();
  }

  public void enableWifi(boolean success) {
    if (success) {
      WifiConnector wifiConnector = new WifiConnector(getActivity(), this);
      wifiConnector.enableWifi();
    } else {
      Toast toast = Toast.makeText(getActivity(), "Cannot Connect to Server without Wifi!", Toast.LENGTH_LONG);
      toast.show();
      getActivity().onBackPressed();
    }
  }

  @Override
  public void wifiEnabledCallback() {
    getUsersFromServer();
  }

  private void getUsersFromServer() {
    // Retrieving list of users from Server
    accListSpinnerArray.clear();

    String urlPrefix = getString(R.string.server_frag_textview_url);
    if (!urlPrefix.endsWith("/"))
      urlPrefix += "/";

    ProgressDialog progDialog = new ProgressDialog(getActivity());
    progDialog.setMessage("Retrieving User Information from Server...");
    progDialog.setIndeterminate(true);
    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    progDialog.setCancelable(true);
    progDialog.setCanceledOnTouchOutside(false);

    final JsonTask jsonTask = new JsonTask(progDialog, METHOD_GET_USERS);
    jsonTask.execute(urlPrefix + "users");

    progDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        jsonTask.cancel(true);
      }
    });
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.login_frag_button_create:
        // Submit EditText email to create new user!
        createUser();
        break;

      case R.id.login_frag_button_signin:
        comm.signinButtonClicked();
        break;

      case R.id.login_frag_button_delete:
        deleteUser(accListSpinner.getSelectedItem().toString());
        break;

      default:
        // Nothing!
    }
  }

  private void createUser() {
    String urlPrefix = getString(R.string.server_frag_textview_url);
    if (!urlPrefix.endsWith("/"))
      urlPrefix += "/";

    ProgressDialog progDialog = new ProgressDialog(getActivity());
    progDialog.setMessage("Connecting to Server to Create New User...");
    progDialog.setIndeterminate(true);
    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    progDialog.setCancelable(true);
    progDialog.setCanceledOnTouchOutside(false);

    final JsonTask jsonTask = new JsonTask(progDialog, METHOD_CREATE_USER);
    jsonTask.execute(urlPrefix + "users");

    progDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        jsonTask.cancel(true);
      }
    });
  }

  private void deleteUser(String email) {
    String urlPrefix = getString(R.string.server_frag_textview_url);
    if (!urlPrefix.endsWith("/"))
      urlPrefix += "/";

    ProgressDialog progDialog = new ProgressDialog(getActivity());
    progDialog.setMessage("Connecting to Server to Delete Selected User...");
    progDialog.setIndeterminate(true);
    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    progDialog.setCancelable(true);
    progDialog.setCanceledOnTouchOutside(false);

    final JsonTask jsonTask = new JsonTask(progDialog, METHOD_DELETE_USER);
    jsonTask.execute(urlPrefix + "users?email=" + email);

    progDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        jsonTask.cancel(true);
      }
    });
  }

  private void refreshSpinner() {
    accListSpinnerArrayAdapter.notifyDataSetChanged();
    accListSpinner.invalidate();

    // By default set the user selected to be the first email in the list
    accListSpinner.setSelection(0);
  }

  // TODO: Should refactor the async methods into a separate utilties class?
  private String asyncDownloadUsers(String... sUrls) {
    InputStream input = null;
    JsonReader jsonReader = null;
    HttpURLConnection connection = null;

    try {
      URL url = new URL(sUrls[0]); //TODO will only be one, but make sure to refactor later
      connection = (HttpURLConnection) url.openConnection();
      connection.connect();

      // expect HTTP 200 OK, so we don't mistakenly save error report
      if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
        return null;
      }

      input = new BufferedInputStream(connection.getInputStream());
      jsonReader = new JsonReader(new InputStreamReader(input));
      jsonReader.beginArray();
      while (jsonReader.hasNext()) {
        String next = jsonReader.nextString();
        accListSpinnerArray.add(next);
      }

    } catch (Exception e) {
      return null;

    } finally {
      try {
        if (input != null)
          input.close();
        if (jsonReader != null)
          jsonReader.close();
      } catch (IOException ignored) {
      }

      if (connection != null)
        connection.disconnect();
    }
    return "Retrieved " + accListSpinnerArray.size() + " users!";
  }

  // TODO: Should refactor the async methods into a separate utilties class?
  private String asyncCreateUser(String... sUrls) {
    HttpURLConnection urlConn = null;

    try {
      URL url = new URL(sUrls[0]);
      urlConn = (HttpURLConnection) url.openConnection();
      urlConn.setRequestMethod("POST");
      urlConn.setDoOutput(true);
      urlConn.setRequestProperty("Content-Type", "application/json");
      urlConn.connect();

      //Create JSONObject here
      JSONObject jsonParam = new JSONObject();
      jsonParam.put("email", newUserEmailEditText.getText().toString());

      // Send POST output.
      DataOutputStream printout = new DataOutputStream(urlConn.getOutputStream());
      printout.writeBytes(jsonParam.toString());
      printout.flush();
      printout.close();

      int httpResult = urlConn.getResponseCode();
      if (httpResult == HttpURLConnection.HTTP_OK)
        return "Created New User " + newUserEmailEditText.getText().toString();
      else
        return "HTTP Reponse: " + urlConn.getResponseMessage();

    } catch (MalformedURLException e) {
      e.printStackTrace();
      return null;
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    } catch (JSONException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (urlConn != null)
        urlConn.disconnect();
    }
  }

  // TODO: Should refactor the async methods into a separate utilties class?
  private String asyncDeleteUser(String... sUrls) {
    HttpURLConnection urlConn = null;

    try {
      URL url = new URL(sUrls[0]);
      urlConn = (HttpURLConnection) url.openConnection();
      urlConn.setRequestMethod("DELETE");
      urlConn.connect();

      int httpResult = urlConn.getResponseCode();
      if (httpResult == HttpURLConnection.HTTP_OK)
        return "Deleted User " + accListSpinner.getSelectedItem().toString();
      else
        return "HTTP Reponse: " + urlConn.getResponseMessage();

    } catch (MalformedURLException e) {
      e.printStackTrace();
      return null;
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (urlConn != null)
        urlConn.disconnect();
    }
  }

  private class JsonTask extends AsyncTask<String, Integer, String> {
    private ProgressDialog progDialog;
    private PowerManager.WakeLock mWakeLock;
    private int method;

    private JsonTask(ProgressDialog progDialog, int method) {
      this.progDialog = progDialog;
      this.method = method;
    }

    @Override
    protected String doInBackground(String... sUrls) {
      switch (method) {
        case METHOD_GET_USERS:
          return asyncDownloadUsers(sUrls);

        case METHOD_CREATE_USER:
          return asyncCreateUser(sUrls);

        case METHOD_DELETE_USER:
          return asyncDeleteUser(sUrls);

        default:
          return null;
      }
    }

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      PowerManager pm = (PowerManager) getActivity().getSystemService(getActivity().POWER_SERVICE);
      mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
        getClass().getName());
      mWakeLock.acquire();
      progDialog.show();
    }

    @Override
    protected void onPostExecute(String result) {
      mWakeLock.release();
      progDialog.dismiss();
      if (result == null)
        Toast.makeText(getActivity(), "Error Connecting to Server!", Toast.LENGTH_LONG).show();
      else {
        Toast.makeText(getActivity(), "Success! " + result, Toast.LENGTH_LONG).show();

        if (method != METHOD_GET_USERS)
          getUsersFromServer();

        refreshSpinner();
      }
    }
  }
}
