package cpen391team2.geocache;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * Created by adam on 16/03/16.
 */
public class DownloadTask extends AsyncTask<String, Integer, String> {
  public interface DownloadListener {
    void onDownloadFinish(boolean success);
  }

  private Context context;
  private DownloadListener listener;
  private ProgressDialog progDialog;
  private Map<String, Long> files;
  private PowerManager.WakeLock mWakeLock;
  private int downloadCount;

  public DownloadTask(Context context, DownloadListener listener,
                      ProgressDialog progDialog, Map<String, Long> files) {
    this.context = context;
    this.listener = listener;
    this.progDialog = progDialog;
    this.files = files;
    downloadCount = 0;
  }

  @Override
  protected String doInBackground(String... sUrls) {
    InputStream input = null;
    OutputStream output = null;
    HttpURLConnection connection = null;

    for (String urlPrefix : sUrls) {
      for (String filename : files.keySet()) {
        long total = 0;
        try {
          URL url = new URL(urlPrefix + filename);
          connection = (HttpURLConnection) url.openConnection();
          connection.connect();

          // expect HTTP 200 OK, so we don't mistakenly save error report
          // instead of the file
          if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            return "Server returned HTTP " + connection.getResponseCode()
              + " " + connection.getResponseMessage();
          }

          // this will be useful to display download percentage
          // might be -1: server did not report the length
          int fileLength = connection.getContentLength();

          // download the file
          input = connection.getInputStream();
          output = context.openFileOutput(filename, context.MODE_PRIVATE);

          byte data[] = new byte[4096];
          int count;
          while ((count = input.read(data)) != -1) {
            // allow canceling with back button
            if (isCancelled()) {
              input.close();
              return "Download Cancelled!";
            }
            total += count;
            // publishing the progress....
            if (fileLength > 0) // only if total length is known
              publishProgress((int) (total * 100 / fileLength));
            output.write(data, 0, count);
          }
        } catch (Exception e) {
          return e.toString();
        } finally {
          try {
            if (output != null)
              output.close();
            if (input != null)
              input.close();
          } catch (IOException ignored) {
          }

          if (connection != null)
            connection.disconnect();

          files.put(filename, total);
          ++downloadCount;
        }
      }
    }
    return null;
  }

  @Override
  protected void onPreExecute() {
    super.onPreExecute();
    // take CPU lock to prevent CPU from going off if the user
    // presses the power button during download
    PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
    mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
      getClass().getName());
    mWakeLock.acquire();
    progDialog.show();
  }

  @Override
  protected void onProgressUpdate(Integer... progress) {
    super.onProgressUpdate(progress);
    // if we get here, length is known, now set indeterminate to false
    progDialog.setIndeterminate(false);
    progDialog.setMax(100);
    progDialog.setProgress(progress[0]);
  }

  @Override
  protected void onPostExecute(String result) {
    mWakeLock.release();
    progDialog.dismiss();
    if (result != null)
      Toast.makeText(context, "Download Failed! " + result, Toast.LENGTH_LONG).show();
    else {
      if (downloadCount != files.size())
        Toast.makeText(context, "Could Not Download All Files!", Toast.LENGTH_LONG).show();
      else {
        Toast.makeText(context, "Downloads successful!", Toast.LENGTH_SHORT).show();
        listener.onDownloadFinish(true);
      }
    }
  }
}
