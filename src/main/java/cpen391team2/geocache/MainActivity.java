package cpen391team2.geocache;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
                          implements CustomDialogFragment.CustomDialogListener,
                                     ServerFragment.ServerFragCommunicator,
                                     BluetoothFragment.BluetoothFragCommunicator,
                                     LoginFragment.LoginFragCommunicator,
                                     UpdateTask.UpdateListener,
                                     DownloadTask.DownloadListener {

  public static final String DE2_RES_PATH = "de2/";
  private static final String CURR_USER_EMAIL = "curr_user_email";

  private int layoutRes;
  private int fileSelectedId;
  private String currPackage;
  private Map<String, Long> fileSizes;

  private String userEmail, cachePackageName;
  private int progress;
  private boolean sendPackage;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    if (fileSizes == null)
      fileSizes = new HashMap<>();

    if (savedInstanceState == null) {
      FragmentTransaction ft = getFragmentManager().beginTransaction();
      ft.replace(R.id.main_act_fragment_container, new LoginFragment());
      ft.commit();
    } else
      userEmail = savedInstanceState.getString(CURR_USER_EMAIL);
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    outState.putString(CURR_USER_EMAIL, userEmail);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    switch (id) {
      case R.id.action_login:
        showLoginFragment();
        break;
      case R.id.action_bluetooth:
        showBluetoothFragment();
        break;
      case R.id.action_server:
        showServerFragment();
        break;
      case R.id.action_exit:
        promptQuit();
        break;
      default:
        //Nothing
    }

    return super.onOptionsItemSelected(item);
  }

  private void showLoginFragment() {
    if (!(getFragmentManager().findFragmentById(R.id.main_act_fragment_container) instanceof LoginFragment))
      onBackPressed();
  }

  private void showServerFragment() {
    if (!(getFragmentManager().findFragmentById(R.id.main_act_fragment_container) instanceof ServerFragment)) {
      FragmentTransaction ft = getFragmentManager().beginTransaction();
      ft.replace(R.id.main_act_fragment_container, new ServerFragment());
      ft.addToBackStack(null);
      ft.commit();
    }
  }

  private void showBluetoothFragment() {
    if (!(getFragmentManager().findFragmentById(R.id.main_act_fragment_container) instanceof BluetoothFragment)) {
      FragmentTransaction ft = getFragmentManager().beginTransaction();
      ft.replace(R.id.main_act_fragment_container, new BluetoothFragment());
      ft.addToBackStack(null);
      ft.commit();
    }
  }

  private void showMapFragment(List<CachePackage> cachePackages) {
    if (!(getFragmentManager().findFragmentById(R.id.main_act_fragment_container) instanceof MyMapFragment)) {
      FragmentTransaction ft = getFragmentManager().beginTransaction();
      ft.replace(R.id.main_act_fragment_container, MyMapFragment.newInstance(cachePackages));
      ft.addToBackStack(null);
      ft.commit();
    }
  }

  private void sendFileThroughBT(String... filenames) {
    Log.i("bttag", "filenames: " + Arrays.toString(filenames));

    Fragment frag = getFragmentManager().findFragmentById(R.id.main_act_fragment_container);

    if (frag instanceof BluetoothFragment)
      ((BluetoothFragment) frag).sendFileToBTDevice(fileSizes, filenames);
    else
      Toast.makeText(this, "Bluetooth Fragment Not Available!", Toast.LENGTH_LONG).show();
  }

  private void promptQuit() {
    Map<CustomDialogFragment.DialogKeys, String> mapBundle = new HashMap<>();
    mapBundle.put(CustomDialogFragment.DialogKeys.TITLE, getString(R.string.dialog_title_quit));
    mapBundle.put(CustomDialogFragment.DialogKeys.MESSAGE, getString(R.string.dialog_message_quit));
    mapBundle.put(CustomDialogFragment.DialogKeys.NEG_BUTTON, getString(R.string.dialog_button_yes));
    mapBundle.put(CustomDialogFragment.DialogKeys.POS_BUTTON, getString(R.string.dialog_button_cancel));

    CustomDialogFragment dialogFrag = CustomDialogFragment.newInstance(mapBundle);
    dialogFrag.show(getFragmentManager(), "Quit Dialog");
  }

  @Override
  public void doButtonClick(String buttonText) {
    if (buttonText.equals(getString(R.string.dialog_button_yes)))
      finish();

    else if (buttonText.equals(getString(R.string.dialog_button_wifi_allow))) {
      Fragment frag = getFragmentManager().findFragmentById(R.id.main_act_fragment_container);
      if (frag instanceof ServerFragment)
        ((ServerFragment) frag).enableWifi(true);
      else if (frag instanceof LoginFragment)
        ((LoginFragment) frag).enableWifi(true);

    } else if (buttonText.equals(getString(R.string.dialog_button_wifi_deny))) {
      Fragment frag = getFragmentManager().findFragmentById(R.id.main_act_fragment_container);
      if (frag instanceof ServerFragment)
        ((ServerFragment) frag).enableWifi(false);
      else if (frag instanceof LoginFragment)
        ((LoginFragment) frag).enableWifi(false);

    } else if (buttonText.equals(getString(R.string.dialog_button_bluetooth_send_file))) {
      String filename;
      switch (fileSelectedId) {
        case R.id.radio_group_file_cache:
          filename = getString(R.string.radio_group_file_cache);
          break;
        case R.id.radio_group_file_map:
          filename = getString(R.string.radio_group_file_map);
          break;
        case R.id.radio_group_file_riddle:
          filename = getString(R.string.radio_group_file_riddle);
          break;
        default:
          return;
      }
      sendFileThroughBT(filename);

    } else if (buttonText.equals(getString(R.string.dialog_button_bluetooth_send_all))) {
      sendFileThroughBT(fileSizes.keySet().toArray(new String[0]));
    }
  }

  /**
   * Overrides back button navigation with fragments
   */
  @Override
  public void onBackPressed() {
    if (getFragmentManager().getBackStackEntryCount() > 0) {
      getFragmentManager().popBackStack();
    } else {
      promptQuit();
    }
  }

  @Override
  public void downloadPackageFiles(String hostname, String packageName, List<String> filenames) {
    sendPackage = true;

    if (hostname == null || hostname.equals("")) {
      displayPackageError(getString(R.string.dialog_title_invalid_hostname), getString(R.string.dialog_button_close));

    } else if (packageName == null || packageName.equals("")) {
      displayPackageError(getString(R.string.dialog_title_invalid_packageName), getString(R.string.dialog_button_close));

    } else if (filenames == null) {
      displayPackageError(getString(R.string.dialog_title_invalid_filenames), getString(R.string.dialog_button_close));

    } else {
      currPackage = packageName;
      fileSizes.clear();
      for (String filename : filenames)
        fileSizes.put(filename, Long.MIN_VALUE);

      if (!hostname.endsWith("/"))
        hostname += "/";
      if (!packageName.endsWith("/"))
        packageName += "/";

      processPackage(filenames, hostname, packageName);
    }
  }

  private void processPackage(List<String> fileNames, String hostname, String packageName) {
    ProgressDialog progDialog = initProgressDialog(fileNames);
    // execute this when the downloader must be fired
    final DownloadTask downloadTask = exeDownloadTask(progDialog, hostname, packageName);

    progDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        downloadTask.cancel(true);
      }
    });
  }

  private ProgressDialog initProgressDialog(List<String> fileNames) {
    ProgressDialog progDialog = new ProgressDialog(this);
    progDialog.setMessage("Downloading files: " + fileNames);
    progDialog.setIndeterminate(true);
    progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    progDialog.setCancelable(true);
    progDialog.setCanceledOnTouchOutside(false);
    return progDialog;
  }

  private DownloadTask exeDownloadTask(ProgressDialog progDialog, String hostname, String packageName) {
    final DownloadTask downloadTask = new DownloadTask(this, this, progDialog, fileSizes);
    downloadTask.execute(hostname + DE2_RES_PATH + packageName);
    return downloadTask;
  }

  @Override
  public void transferUserInfo() {
    sendPackage = false;

    showBluetoothFragment();
  }

  private void displayPackageError(String errorTitle, String buttonPosition) {
    Map<CustomDialogFragment.DialogKeys, String> mapBundle = new HashMap<>();
    mapBundle.put(CustomDialogFragment.DialogKeys.TITLE, errorTitle);
    mapBundle.put(CustomDialogFragment.DialogKeys.POS_BUTTON, buttonPosition);

    CustomDialogFragment dialogFrag = CustomDialogFragment.newInstance(mapBundle);
    dialogFrag.show(getFragmentManager(), "Invalid Data to Transfer Dialog");
  }

  @Override
  public void onFileTransferComplete(String err) {
    if (err != null)
      Toast.makeText(this, "File Transfer failed! Error: " + err, Toast.LENGTH_LONG).show();

    onBackPressed();
  }

  @Override
  public void onReceiveUserInfoComplete(String err) {
    if (err == null) {
      // Execute an AsyncTask for updating server
      ProgressDialog progDialog = new ProgressDialog(this);
      progDialog.setMessage("Updating Server with User Progress Received from DE2...");
      progDialog.setIndeterminate(true);
      progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
      progDialog.setCancelable(true);
      progDialog.setCanceledOnTouchOutside(false);

      final UpdateTask task = new UpdateTask(this, this, progDialog, userEmail, cachePackageName, progress);
      task.execute(getString(R.string.server_frag_textview_url) + "users");
    } else {
      Toast.makeText(this, "Receive User Info failed! Error: " + err, Toast.LENGTH_LONG).show();
      onBackPressed();
    }
  }

  @Override
  public void onDownloadFinish(boolean success) {
    if (success) {
      showBluetoothFragment();
    }
  }

  @Override
  public void onUpdateFinish(boolean success) {
    if (success)
      Toast.makeText(this, "Successfully Updated the Server with New Progress!", Toast.LENGTH_LONG).show();

    onBackPressed();
  }

  @Override
  public void onBluetoothConnected() {
    if (sendPackage) {
      // BluetoothConnected for Sending Cache Package Files to DE2
      Map<CustomDialogFragment.DialogKeys, String> mapBundle = initDownloadFinishMapBundle();

      CustomDialogFragment dialogFrag = CustomDialogFragment.newInstance(mapBundle);
      dialogFrag.show(getFragmentManager(), "Bluetooth Send Dialog");

    } else {
      // BluetoothConnected for Receiving User Info
      Fragment frag = getFragmentManager().findFragmentById(R.id.main_act_fragment_container);

      if (frag instanceof BluetoothFragment) {
        ((BluetoothFragment) frag).receiveProgress();
      } else
        Toast.makeText(this, "Bluetooth Fragment Not Available!", Toast.LENGTH_LONG).show();
    }
  }

  public Map<CustomDialogFragment.DialogKeys, String> initDownloadFinishMapBundle() {
    Map<CustomDialogFragment.DialogKeys, String> mapBundle = new HashMap<>();
    mapBundle.put(CustomDialogFragment.DialogKeys.TITLE, getString(R.string.dialog_bluetooth_title));
    mapBundle.put(CustomDialogFragment.DialogKeys.MESSAGE, getString(R.string.dialog_bluetooth_message));
    mapBundle.put(CustomDialogFragment.DialogKeys.NEG_BUTTON, getString(R.string.dialog_button_bluetooth_send_all));
    mapBundle.put(CustomDialogFragment.DialogKeys.NEU_BUTTON, getString(R.string.dialog_button_bluetooth_send_file));
    mapBundle.put(CustomDialogFragment.DialogKeys.POS_BUTTON, getString(R.string.dialog_button_cancel));
    mapBundle.put(CustomDialogFragment.DialogKeys.NEU_BUTTON_CALLBACK, "true");
    mapBundle.put(CustomDialogFragment.DialogKeys.CUSTOM_LAYOUT_RES, "true");
    layoutRes = R.layout.diag_frag_choose_file;
    return mapBundle;
  }

  @Override
  public void signinButtonClicked() {
    showServerFragment();
  }

  @Override
  public void requestWifiEnable() {
    Map<CustomDialogFragment.DialogKeys, String> mapBundle = initWifiMapBundle();

    CustomDialogFragment dialogFrag = CustomDialogFragment.newInstance(mapBundle);
    dialogFrag.show(getFragmentManager(), "Wifi Dialog");
  }

  @Override
  public void showMapView(List<CachePackage> cachePackages) {
    showMapFragment(cachePackages);
  }

  private Map<CustomDialogFragment.DialogKeys, String> initWifiMapBundle() {
    Map<CustomDialogFragment.DialogKeys, String> mapBundle = new HashMap<>();
    mapBundle.put(CustomDialogFragment.DialogKeys.TITLE, getString(R.string.dialog_title_wifi_request));
    mapBundle.put(CustomDialogFragment.DialogKeys.MESSAGE, getString(R.string.dialog_message_wifi_request));
    mapBundle.put(CustomDialogFragment.DialogKeys.NEG_BUTTON, getString(R.string.dialog_button_wifi_allow));
    mapBundle.put(CustomDialogFragment.DialogKeys.POS_BUTTON, getString(R.string.dialog_button_wifi_deny));
    mapBundle.put(CustomDialogFragment.DialogKeys.POS_BUTTON_CALLBACK, "true");
    return mapBundle;
  }

  public int getLayoutRes() {
    return layoutRes;
  }

  public void setFileSelectedId(int radioId) {
    fileSelectedId = radioId;
  }

  public String getCurrPackage() {
    return currPackage;
  }

  public String getUserEmail() { return userEmail; }

  public void setUserEmail(String newEmail) { userEmail = newEmail; }

  public int getProgress() { return progress; }

  public void setPackageProgress(int progress) { this.progress = progress; }

  public String getCachePackageName() { return cachePackageName; }

  public void setCachePackageName(String name) { this.cachePackageName = name; }
}
