package cpen391team2.geocache;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by adam on 07/04/16.
 */
public class UpdateTask extends AsyncTask<String, Integer, String> {
  public interface UpdateListener {
    void onUpdateFinish(boolean success);
  }


  private Context context;
  private UpdateListener listener;
  private ProgressDialog progDialog;
  private PowerManager.WakeLock mWakeLock;

  private String email, packageName;
  private int progress;

  public UpdateTask(Context context, UpdateListener listener, ProgressDialog progDialog,
                    String email, String packageName, int progress) {
    this.context = context;
    this.listener = listener;
    this.progDialog = progDialog;
    this.email = email;
    this.packageName = packageName;
    this.progress = progress;
  }

  @Override
  protected String doInBackground(String... sUrls) {
    HttpURLConnection urlConn = null;

    try {
      URL url = new URL(sUrls[0]);
      urlConn = (HttpURLConnection) url.openConnection();
      urlConn.setRequestMethod("POST");
      urlConn.setDoOutput(true);
      urlConn.setRequestProperty("Content-Type", "application/json");
      urlConn.connect();

      //Create JSONObject here
      JSONObject jsonParam = new JSONObject();
      jsonParam.put("email", email);
      jsonParam.put(packageName, progress);

      // Send POST output.
      DataOutputStream printout = new DataOutputStream(urlConn.getOutputStream());
      printout.writeBytes(jsonParam.toString());
      printout.flush();
      printout.close();

      int httpResult = urlConn.getResponseCode();
      if (httpResult == HttpURLConnection.HTTP_OK)
        return null;
      else
        return "HTTP Reponse: " + urlConn.getResponseMessage();

    } catch (MalformedURLException e) {
      e.printStackTrace();
      return null;
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    } catch (JSONException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (urlConn != null)
        urlConn.disconnect();
    }
  }

  @Override
  protected void onPreExecute() {
    super.onPreExecute();
    // take CPU lock to prevent CPU from going off if the user
    // presses the power button during download
    PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
    mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
      getClass().getName());
    mWakeLock.acquire();
    progDialog.show();
  }

  @Override
  protected void onPostExecute(String result) {
    mWakeLock.release();
    progDialog.dismiss();
    if (result != null)
      Toast.makeText(context, "Update Failed! " + result, Toast.LENGTH_LONG).show();
    else {
      listener.onUpdateFinish(true);
    }
  }
}
